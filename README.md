# Clarification Meetings

A clarification meeting is a public 
meeting where the procuring entity answer questions about an on going tender.
This extension adds information about the date and place where clarification 
meetings for a tender process take place. 
As multiples clarification meetings for a tender process can exist, 
the extension adds an array of `clarificationMeetings` at tender level with an 
id for unique identify the clarification object inside its array.
The address block is used to indicate where the clarification meeting take place. 
If only a free text is availabe then you can use the `streetAddress` field 
inside the address block to put the free text content.

## Example

```json
{
    "tender": {
        "clarificationMeetings": [
            {
                "address": {
                    "streetAddress": "Street 123"
                },
                "date": "2017-08-30T07:39:57Z"
                "id": "1"
            }
        ]
    }
}

```